# zthumb

a bash script that helps you making thumbnails from any pictures or video files

usage

```sh
zthumb.sh <filename>
```

and it will create a thumbnail in the folder where you are calling `zthumb.sh`

## batch

if you wanna use it on many files, just use `find` :

```sh
find -type f -exec zthumb.sh {} \;
```

## more demos

make single thumb in the current dir (300 x 300 px) :
```sh
zthumb.sh "IMG_6406.cr2";
```

make single 500x500 thumb in the current dir :
```sh
zthumb.sh "IMG_6406.cr2" "500x500";
```

loads of thumbs in the current dir :
```sh
find -type f -iname "*.jpg" -exec zthumb.sh {} \;
```

loads of 500x500 thumbs in the current dir :
```sh
find -type f -iname "*.jpg" -exec zthumb.sh {} "500x500"\;
```

loads of thumbs from a specific input folder : 
```sh
find /media/canon/ -type f -iname "*.jpg" -exec zthumb.sh {} \;
```

rough input extensions selection, using find :
```sh
find -type f ( -iname "*.mov" -o -iname "*.mp4" -o -iname "*.jpg" -o -iname "*.jpeg" -o -iname "*.gif" -o -iname "*.dng" -o -iname "*.cr2" -o -iname "*.tif" -o -iname "*.tiff" \) -exec zthumb.sh {} \;
```

## credits

find me on my [/now](https://humanize.me/now/) page