#!/bin/bash

# ------------------------------------------------------
# USING FROM ANY TERMINAL
# copy that script in /usr/local/bin
# so you can run it from everywhere :)
# ------------------------------------------------------
# sudo cp zthumb.sh /usr/local/bin/zthumb.sh; sudo chmod 555 /usr/local/bin/zthumb.sh

# ------------------------------------------------------
# undefined input checkpoint
# ------------------------------------------------------
if [ -z "${1}" ]; then
  echo "zthumb v0.1b | MIT Licence | www.humanize.me"
  echo "making thumbnails from your pictures & videos"
  echo ""
  echo "please note that zthumb will save the thumbnails in the folder where it has been called !"
  echo ""
  echo "SINGLE FILE"
  echo "usage : zthumb.sh <input> (<maxsize>)"
  echo "demo1 : zthumb.sh \"IMG_6406.cr2\";"
  echo "demo2 : zthumb.sh \"IMG_6406.cr2\" \"500x500\";"
  echo ""
  echo "MULTIPLE FILES"
  echo "usage : find -type f -exec zthumb.sh {} \;"
  echo "demo1 : find -type f -iname \"*.jpg\" -exec zthumb.sh {} \;"
  echo "demo2 : find /YOUR_SOURCE_FOLDER/ -type f -iname \"*.jpg\" -exec zthumb.sh {} \;"
  echo "demo3 : find -type f -iname \"*.jpg\" -exec zthumb.sh {} \"500x500\"\;"
  echo "demo4 : find -type f ( -iname \"*.mov\" -o -iname \"*.mp4\" -o -iname \"*.jpg\" -o -iname \"*.jpeg\" -o -iname \"*.gif\" -o -iname \"*.dng\" -o -iname \"*.cr2\" -o -iname \"*.tif\" -o -iname \"*.tiff\" \) -exec zthumb.sh {} \;"
  echo ""
  exit
fi

# ------------------------------------------------------
# prerequisites
# ------------------------------------------------------
command -v ART-cli >/dev/null 2>&1 || {
  echo >&2 "I require art but it's not installed. Aborting.";
  echo "how to install it :";
  echo "sudo add-apt-repository ppa:dhor/myway"
  echo "sudo apt update; sudo apt install art;"
  exit 1;
}
command -v convert >/dev/null 2>&1 || {
  echo >&2 "I require convert from imagemagick but it's not installed. Aborting.";
  echo "how to install it :";
  echo "sudo apt install imagemagick;"
  exit 1;
}

# ------------------------------------------------------
# color helpers
# ------------------------------------------------------
green="\033[1;32m"
reset="\033[0m"

# ------------------------------------------------------
# input modifier (if using find)
# ------------------------------------------------------
first_chars=$(echo "${1}" | cut -c -2);
if [ $first_chars = "./" ]; then
  # echo "looks like you're using find :B";
  input=$(echo "${1}" | cut -c 3-);
else
  input=$1;
fi

# ------------------------------------------------------
# input filename variables
# ------------------------------------------------------
input_basename="${input##*/}"
input_path=$(dirname "${input}")
input_filename=$(basename "${input%.*}") # oh yeah
input_extension="${input_basename##*.}"
input_extension=${input_extension,,} # lowercase

# ------------------------------------------------------
# max_size
# ------------------------------------------------------
if [ -z $2 ]; then
  max_size="300x300"
else
  max_size=$2
fi

# ------------------------------------------------------
# process
# ------------------------------------------------------
output_basename="zth--${input_basename}-${max_size}.jpg"

if [[ ! -f $output_basename ]]; then
  
  # thumb filename doesn't exist

  if [ "${input_extension}" = "cr3" ] || [ "${input_extension}" = "cr2" ] || [ "${input_extension}" = "dng" ]; then

    # input is a picture (raw format)
    # this file needs to be pre-converted using ART

    tmp_basename="${input_filename}--tmp.jpg"

    # quiet mode
    ART-cli -q -o "$tmp_basename" -c "$input" # options -o must be set before -c (ok then, lol)
    convert "$tmp_basename" -auto-orient -strip -thumbnail "$max_size" -quality 50 -colorspace sRGB "$output_basename"
    rm "${tmp_basename}"

  elif [ "${input_extension}" = "mp4" ] || [ "${input_extension}" = "mpv" ] || [ "${input_extension}" = "ts" ] || [ "${input_extension}" = "mov" ] || [ "${input_extension}" = "avi" ] || [ "${input_extension}" = "ogv" ] || [ "${input_extension}" = "webm" ] || [ "${input_extension}" = "mpeg" ] || [ "${input_extension}" = "3gp" ] || [ "${input_extension}" = "mkv" ]; then

    # input is a video
    # a frame needs to be extracted using ffmpeg

    tmp_basename="${input_filename}--tmp.png"
    ffmpeg -loglevel error -ss 0:0:1 -i "${input}" -frames:v 1 ${tmp_basename}
    convert "$tmp_basename" -auto-orient -strip -thumbnail "$max_size" -quality 50 -colorspace sRGB "$output_basename"
    rm "${tmp_basename}"

  else

    # not a picture (raw format) neither a video, so ..
    # let's just resize it using imagemagick

    convert "${input}" -auto-orient -strip -thumbnail "$max_size" -quality 50 -colorspace sRGB "$output_basename"

  fi

  echo -e "${green}[in]${reset} ${input_basename} ${green}[out]${reset} ${output_basename}"

else

  # thumb already exists
  
  echo -e "${green}[in]${reset} ${input_basename} ${green}[skip]${reset} ${output_basename}"

fi